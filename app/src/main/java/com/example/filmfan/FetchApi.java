package com.example.filmfan;

import com.example.filmfan.Models.Movie;
import com.example.filmfan.Models.Rate;
import com.example.filmfan.Responses.CastsResponse;
import com.example.filmfan.Responses.GenresResponse;
import com.example.filmfan.Responses.MoviesResponse;
import com.example.filmfan.Responses.RateMovieResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FetchApi {
    @GET("movie/now_playing")
    Call<MoviesResponse> getNowPlayingMovies(
            @Query("api_key") String apiKey,
            @Query("page") int page
    );

    @GET("movie/{movie_id}/recommendations")
    Call<MoviesResponse> getRecommendedMovies(
            @Path("movie_id") int id,
            @Query("api_key") String apiKey,
            @Query("page") int page
    );

    @GET("genre/movie/list")
    Call<GenresResponse> getGenres(
            @Query("api_key") String apiKey,
            @Query("language") String language
    );

    @GET("movie/{movie_id}")
    Call<Movie> getMovie(
            @Path("movie_id") int id,
            @Query("api_key") String apiKEy,
            @Query("language") String language
    );

    @GET("movie/{movie_id}/credits")
    Call<CastsResponse> getCasts(
            @Path("movie_id") int id,
            @Query("api_key") String apiKEy
    );

    @Headers("Content-Type:application/json;charset=utf-8")
    @POST("movie/{movie_id}/rating")
    Call<RateMovieResponse> rateMovie(
            @Path("movie_id") int id,
            @Query("api_key") String apiKEy,
            @Query("guest_session_id") String sessionId,
            @Body Rate rate
    );
}

