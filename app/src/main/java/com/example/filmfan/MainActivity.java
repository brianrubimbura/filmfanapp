package com.example.filmfan;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.filmfan.Adapters.MoviesAdapter;
import com.example.filmfan.Contracts.OnGetGenresCallback;
import com.example.filmfan.Contracts.OnGetMoviesCallback;
import com.example.filmfan.Contracts.OnMoviesClickCallback;
import com.example.filmfan.Models.Genre;
import com.example.filmfan.Models.Movie;
import com.example.filmfan.Services.MoviesService;

import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView moviesList;
    private MoviesAdapter adapter;
    private SwipeRefreshLayout mySwipeRefreshLayout;
    private MoviesService moviesService;
    private ImageButton favouriteButton;

    private List<Genre> movieGenres;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        moviesService = MoviesService.getInstance();
        mySwipeRefreshLayout = findViewById(R.id.swiperefresh);

        favouriteButton =findViewById(R.id.favourite_button);

        mySwipeRefreshLayout.setRefreshing(true);

        setTitle("Now Playing Movies");



        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getGenres();
                    }
                });

        moviesList = findViewById(R.id.movies_list);
        moviesList.setLayoutManager(new LinearLayoutManager(this));

        getGenres();
    }


    private void getGenres() {
        moviesService.getGenres(new OnGetGenresCallback() {
            @Override
            public void onSuccess(List<Genre> genres) {
                Log.i("Genres", String.valueOf(genres.size()));
                movieGenres = genres;
                getMovies();

                if (mySwipeRefreshLayout.isRefreshing()) {
                    mySwipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onError() {
                showError();
            }
        });
    }

    OnMoviesClickCallback callback = new OnMoviesClickCallback() {
        @Override
        public void onClick(Movie movie) {
            Intent intent = new Intent(MainActivity.this, MovieActivity.class);
            intent.putExtra(MovieActivity.MOVIE_ID, movie.getId());
            startActivity(intent);
        }
    };

    private void getMovies() {
        mySwipeRefreshLayout.setRefreshing(true);

        moviesService.getNowPlayingMovies(1, new OnGetMoviesCallback() {
            @Override
            public void onSuccess(List<Movie> movies) {
                Log.i("MoviesLog", "Success Number :" + movies.size());
                Collections.sort(movies);
                adapter = new MoviesAdapter(getApplicationContext(),movies, movieGenres, callback);
                moviesList.setAdapter(adapter);
                mySwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onError() {
                showError();
            }
        });
    }


    private void showError() {
        Toast.makeText(MainActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_movies, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                mySwipeRefreshLayout.setRefreshing(true);
                getGenres();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
