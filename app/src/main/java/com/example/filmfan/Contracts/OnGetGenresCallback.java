package com.example.filmfan.Contracts;

import com.example.filmfan.Models.Genre;

import java.util.List;

public interface OnGetGenresCallback {
    void onSuccess(List<Genre> genres);

    void onError();
}
