package com.example.filmfan.Contracts;

import com.example.filmfan.Models.Cast;

import java.util.List;

public interface OnGetCastsCallback {
    void onSuccess(List<Cast> casts);

    void onError();
}
