package com.example.filmfan.Contracts;

import com.example.filmfan.Models.Movie;

public interface OnMoviesClickCallback {
    void onClick(Movie movie);
}
