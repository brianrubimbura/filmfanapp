package com.example.filmfan.Contracts;

import com.example.filmfan.Models.Movie;

import java.util.List;

public interface OnGetMoviesCallback {
    void onSuccess(List<Movie> movies);

    void onError();
}
