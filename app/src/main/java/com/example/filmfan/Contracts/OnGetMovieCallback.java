package com.example.filmfan.Contracts;

import com.example.filmfan.Models.Movie;

public interface OnGetMovieCallback {

    void onSuccess(Movie movie);

    void onError();
}
