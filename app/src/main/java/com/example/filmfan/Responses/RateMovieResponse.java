package com.example.filmfan.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RateMovieResponse {
    @SerializedName("status_code")
    @Expose
    private int statusCode;

    @SerializedName("status_message")
    @Expose
    private String message;

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
