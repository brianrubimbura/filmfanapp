package com.example.filmfan.Responses;

import com.example.filmfan.Models.Cast;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CastsResponse {
    @SerializedName("cast")
    @Expose
    private List<Cast> casts;

    public List<Cast> getCasts() {
        return casts;
    }

    public void setCasts(List<Cast> casts) {
        this.casts = casts;
    }


}
