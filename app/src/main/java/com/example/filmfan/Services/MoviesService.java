package com.example.filmfan.Services;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.filmfan.Contracts.OnGetCastsCallback;
import com.example.filmfan.Contracts.OnGetGenresCallback;
import com.example.filmfan.Contracts.OnGetMovieCallback;
import com.example.filmfan.Contracts.OnGetMoviesCallback;
import com.example.filmfan.FetchApi;
import com.example.filmfan.Models.Movie;
import com.example.filmfan.Models.Rate;
import com.example.filmfan.Responses.CastsResponse;
import com.example.filmfan.Responses.GenresResponse;
import com.example.filmfan.Responses.MoviesResponse;
import com.example.filmfan.Responses.RateMovieResponse;
import com.example.filmfan.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MoviesService {

    private static final String BASE_URL = "https://api.themoviedb.org/3/";
    private static final String LANGUAGE = "en-US";

    private static MoviesService service;

    private FetchApi api;

    private MoviesService(FetchApi api) {
        this.api = api;
    }

    public static MoviesService getInstance() {
        if (service == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            service = new MoviesService(retrofit.create(FetchApi.class));
        }

        return service;
    }

    public void getNowPlayingMovies(int page, final OnGetMoviesCallback callback) {
        api.getNowPlayingMovies(Utils.API_KEY, page)
                .enqueue(new Callback<MoviesResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<MoviesResponse> call, @NonNull Response<MoviesResponse> response) {
                        if (response.isSuccessful()) {
                            MoviesResponse moviesResponse = response.body();
                            if (moviesResponse != null && moviesResponse.getMovies() != null) {
                                callback.onSuccess(moviesResponse.getMovies());
                            } else {
                                callback.onError();
                            }
                        } else {
                            callback.onError();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<MoviesResponse> call, @NonNull Throwable t) {
                        callback.onError();
                    }
                });
    }

    public void getRecommendedMovies(int movieId,int page, final OnGetMoviesCallback callback) {
        api.getRecommendedMovies(movieId,Utils.API_KEY, page)
                .enqueue(new Callback<MoviesResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<MoviesResponse> call, @NonNull Response<MoviesResponse> response) {
                        if (response.isSuccessful()) {
                            Log.i("Rec","Succ On");
                            MoviesResponse moviesResponse = response.body();
                            if (moviesResponse != null && moviesResponse.getMovies() != null) {
                                callback.onSuccess(moviesResponse.getMovies());
                            } else {
                                callback.onError();
                            }
                        } else {
                            callback.onError();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<MoviesResponse> call, @NonNull Throwable t) {
                        callback.onError();
                    }
                });
    }

    public void getGenres(final OnGetGenresCallback callback) {
        api.getGenres(Utils.API_KEY, LANGUAGE)
                .enqueue(new Callback<GenresResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<GenresResponse> call,
                                           @NonNull Response<GenresResponse> response) {
                        if (response.isSuccessful()) {
                            GenresResponse genresResponse = response.body();
                            if (genresResponse != null && genresResponse.getGenres() != null) {
                                callback.onSuccess(genresResponse.getGenres());
                            } else {
                                callback.onError();
                            }
                        } else {
                            callback.onError();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<GenresResponse> call,
                                          @NonNull Throwable t) {
                        callback.onError();
                    }
                });
    }

    public void getMovie(int movieId, final OnGetMovieCallback callback) {
        api.getMovie(movieId, Utils.API_KEY, LANGUAGE)
                .enqueue(new Callback<Movie>() {
                    @Override
                    public void onResponse(@NonNull Call<Movie> call, @NonNull Response<Movie> response) {
                        if (response.isSuccessful()) {
                            Movie movie = response.body();
                            if (movie != null) {
                                callback.onSuccess(movie);
                            } else {
                                callback.onError();
                            }
                        } else {
                            callback.onError();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<Movie> call, @NonNull Throwable t) {
                        callback.onError();
                    }
                });
    }


    public void getCharacters(int movieId, final OnGetCastsCallback callback) {
        api.getCasts(movieId, Utils.API_KEY)
                .enqueue(new Callback<CastsResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<CastsResponse> call,
                                           @NonNull Response<CastsResponse> response) {
                        if (response.isSuccessful()){
                            CastsResponse castsResponse =response.body();
                            if(castsResponse !=null && castsResponse.getCasts()!=null){
                                callback.onSuccess(castsResponse.getCasts());
                            }else{
                                callback.onError();
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CastsResponse> call, @NonNull Throwable t) {
                        callback.onError();
                    }

                });
    }
    public void rateMovie(int movieId,Rate rate, Callback<RateMovieResponse> callback) {
        Call<RateMovieResponse> rateCall = api.rateMovie(movieId,Utils.API_KEY,Utils.SESSION_ID,rate);
        rateCall.enqueue(callback);
    }
}