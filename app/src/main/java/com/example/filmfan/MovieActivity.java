package com.example.filmfan;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.filmfan.Contracts.OnGetCastsCallback;
import com.example.filmfan.Contracts.OnGetGenresCallback;
import com.example.filmfan.Contracts.OnGetMovieCallback;
import com.example.filmfan.Contracts.OnGetMoviesCallback;
import com.example.filmfan.Models.Cast;
import com.example.filmfan.Models.Genre;
import com.example.filmfan.Models.Movie;
import com.example.filmfan.Models.Rate;
import com.example.filmfan.Responses.RateMovieResponse;
import com.example.filmfan.Services.MoviesService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieActivity extends AppCompatActivity {
    public static String MOVIE_ID = "movie_id";
    private static String IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w780";
    private ImageView movieBackdrop;
    private TextView movieTitle;
    private TextView movieGenres;
    private TextView movieOverview;
    private TextView movieOverviewLabel;
    private TextView movieReleaseDate;
    private RatingBar movieRating;
    private LinearLayout recommendedLayout;

    private TextView movieActorsAndCharactersLabel;
    private TextView movieCharactersLabel;
    private TextView movieCharacters;
    private TextView movieActorsLabel;
    private TextView movieActors;
    private TextView movieRateLabel;
    private RatingBar movieRatingBar;
    private Button btnSubmit;

    private TextView recommendedLabel;

    private MoviesService moviesService;
    private int movieId;
    private float ratingValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);

        movieId = getIntent().getIntExtra(MOVIE_ID, movieId);

        moviesService = MoviesService.getInstance();

        setupToolbar();

        initUI();

        getMovie();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }


    private void initUI() {
        movieBackdrop = findViewById(R.id.movieDetailsBackdrop);

        movieTitle = findViewById(R.id.movieDetailsTitle);
        movieGenres = findViewById(R.id.movieDetailsGenres);
        movieOverview = findViewById(R.id.movieDetailsOverview);
        movieOverviewLabel = findViewById(R.id.summaryLabel);
        movieReleaseDate = findViewById(R.id.movieDetailsReleaseDate);
        movieRating = findViewById(R.id.movieDetailsRating);
        recommendedLayout = findViewById(R.id.movieTrailers);


        recommendedLabel = findViewById(R.id.recommendedLabel);


        movieActorsAndCharactersLabel = findViewById(R.id.movieDetailsActorsCharactersLabel);
        movieCharactersLabel = findViewById(R.id.movieDetailsCharactersLabel);
        movieCharacters = findViewById(R.id.movieDetailsCharacters);
        movieActorsLabel = findViewById(R.id.movieDetailsActorsLabel);
        movieActors = findViewById(R.id.movieDetailsActors);
        movieRateLabel = findViewById(R.id.movieRateLabel);
        movieRatingBar = findViewById(R.id.ratingBar);
        btnSubmit = findViewById(R.id.submitRate);

        ProgressBar progressBar = new ProgressBar(getApplicationContext());
        progressBar.setIndeterminate(true);
        movieRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                ratingValue = rating;
            }
        });
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ratingValue = movieRatingBar.getRating();
                rateMovie(ratingValue);
            }
        });
    }

    private void rateMovie(float value) {

        moviesService.rateMovie(movieId, new Rate(value), new Callback<RateMovieResponse>() {
            @Override
            public void onResponse(@NonNull Call<RateMovieResponse> call, @NonNull Response<RateMovieResponse> response) {
                RateMovieResponse movieResponse = response.body();
                if (response.isSuccessful() && movieResponse != null) {
                    Toast.makeText(MovieActivity.this, "Thank you for rating movie", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Error Rating Movie", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RateMovieResponse> call, @NonNull Throwable t) {
                Toast.makeText(getApplicationContext(), "Error Rating Movie", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMovie() {
        moviesService.getMovie(movieId, new OnGetMovieCallback() {
            @Override
            public void onSuccess(Movie movie) {
                movieTitle.setText(movie.getTitle());
                movieOverviewLabel.setVisibility(View.VISIBLE);
                movieOverview.setText(movie.getOverview());
                movieRating.setVisibility(View.VISIBLE);
                movieRating.setRating(movie.getRating() / 2);
                getGenres(movie);
                movieReleaseDate.setText(movie.getReleaseDate().split("-")[0]);
                if (!isFinishing()) {
                    Glide.with(MovieActivity.this)
                            .load(IMAGE_BASE_URL + movie.getBackdrop())
                            .apply(RequestOptions.placeholderOf(R.color.colorPrimary).error(R.color.colorAccent))
                            .into(movieBackdrop);
                }

                getCasts(movie);
                getRecommendedMovies(movie);
            }

            @Override
            public void onError() {
                finish();
            }
        });
    }


    private void getCasts(final Movie movie) {
        moviesService.getCharacters(movie.getId(), new OnGetCastsCallback() {
            @Override
            public void onSuccess(List<Cast> casts) {
                movieActorsAndCharactersLabel.setVisibility(View.VISIBLE);
                movieCharactersLabel.setVisibility(View.VISIBLE);
                movieActorsLabel.setVisibility(View.VISIBLE);
                movieRateLabel.setVisibility(View.VISIBLE);
                movieRatingBar.setVisibility(View.VISIBLE);
                btnSubmit.setVisibility(View.VISIBLE);

                StringBuilder characters = new StringBuilder();
                StringBuilder actors = new StringBuilder();
                for (Cast cast : casts) {
                    if (characters.length() == 0) {
                        characters = new StringBuilder(cast.getCharacter());
                        actors = new StringBuilder(cast.getName());
                    } else {
                        characters.append(",").append(cast.getCharacter());
                        actors.append(",").append(cast.getName());
                    }
                }
                movieCharacters.setText(characters.toString());
                movieActors.setText(actors.toString());
            }

            @Override
            public void onError() {
                Toast.makeText(getApplicationContext(), "Error getting characters", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void getRecommendedMovies(final Movie movie) {
        moviesService.getRecommendedMovies(movie.getId(), 1, new OnGetMoviesCallback() {
            @Override
            public void onSuccess(List<Movie> movies) {
                recommendedLabel.setVisibility(View.VISIBLE);
                recommendedLayout.removeAllViews();

                for (final Movie mv : movies) {
                    View parent = getLayoutInflater().inflate(R.layout.movie_recommended, recommendedLayout, false);
                    ImageView thumbnail = parent.findViewById(R.id.thumbnail);
                    thumbnail.requestLayout();

                    TextView movieName = parent.findViewById(R.id.movieName);
                    movieName.requestLayout();

                    movieName.setText(mv.getTitle());

                    Glide.with(getApplicationContext())
                            .load(IMAGE_BASE_URL + mv.getPosterPath())
                            .apply(RequestOptions.placeholderOf(R.color.colorPrimary)
                                    .error(R.color.colorAccent))
                            .fitCenter()
                            .dontAnimate()
                            .into(thumbnail);
                    recommendedLayout.addView(parent);
                }
            }

            @Override
            public void onError() {
                recommendedLabel.setVisibility(View.GONE);
            }
        });
    }

    private void getGenres(final Movie movie) {
        moviesService.getGenres(new OnGetGenresCallback() {
            @Override
            public void onSuccess(List<Genre> genres) {
                if (movie.getGenres() != null) {
                    List<String> currentGenres = new ArrayList<>();
                    for (Genre genre : movie.getGenres()) {
                        currentGenres.add(genre.getName());
                    }
                    movieGenres.setText(TextUtils.join(", ", currentGenres));
                }
            }

            @Override
            public void onError() {
                showError();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void showError() {
        Toast.makeText(MovieActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
    }
}
